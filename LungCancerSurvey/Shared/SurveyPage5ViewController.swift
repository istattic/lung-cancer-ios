//
//  SurveyPage5ViewController.swift
//  LungCancerSurvey
//
//  Created by Jonah on 6/9/21.
//

import Foundation
import UIKit

class SurveyPage5ViewController: UIViewController{
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var label: UILabel!
    var questionIndex = 0
    
    @IBAction func selectButton1(_ sender: Any){
        button1.setTitle("Not At All", for: UIControl.State.normal)
        GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Not At All, "
        button2.isHidden = false
        button3.isHidden = false
        button4.isHidden = false
        button5.isHidden = false
        questionIndex += 1
        UpdateLabel()
    }
    @IBAction func selectButton2(_ sender: Any){
        GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "A Little, "
        questionIndex += 1
        UpdateLabel()
        GlobalVariables.SymptomTotalRating += 1
    }
    @IBAction func selectButton3(_ sender: Any){
        GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Somewhat, "
        questionIndex += 1
        UpdateLabel()
        GlobalVariables.SymptomTotalRating += 2
    }
    @IBAction func selectButton4(_ sender: Any){
        GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Quite a Bit, "
        questionIndex += 1
        UpdateLabel()
        GlobalVariables.SymptomTotalRating += 3
    }
    @IBAction func selectButton5(_ sender: Any){
        GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Very Much, "
        questionIndex += 1
        UpdateLabel()
        GlobalVariables.SymptomTotalRating += 4
    }
    func UpdateLabel(){
        if (questionIndex == 1){
            label.text = "Lack of Energy"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Overall Feeling/Fatigue: "
        }
        if (questionIndex == 2){
            label.text = "Nausea"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Nausea: "
        }
        if (questionIndex == 3){
            label.text = "Trouble meeting needs of my family"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Trouble meeting needs of my family: "
        }
        if (questionIndex == 4){
            label.text = "Pain"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Pain: "
        }
        if (questionIndex == 5){
            label.text = "Bothered by treatment side effects"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Bothered by treatment side effects: "
        }
        if (questionIndex == 6){
            label.text = "Feel Ill"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Feel Ill: "
        }
        if (questionIndex == 7){
            label.text = "Forced to spend time in bed"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Forced to spend time in bed: "
        }
        if (questionIndex == 8){
            label.text = "Not able to work"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Not able to work: "
        }
        if (questionIndex == 9){
            label.text = "My work isn't fulfilling"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "My work isn't fulfilling: "
        }
        if (questionIndex == 10){
            label.text = "Not able to enjoy life"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Not able to enjoy life: "
        }
        if (questionIndex == 11){
            label.text = "I haven't accepted my illness"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "I haven't accepted my illness: "
        }
        if (questionIndex == 12){
            label.text = "I am not sleeping well"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "I am not sleeping well: "
        }
        if (questionIndex == 13){
            label.text = "I am not enjoying the things I usually do for fun"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "I am not enjoying the things I usually do for fun: "
        }
        if (questionIndex == 14){
            label.text = "I am not content with my current quality of life"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "I am not content with my current quality of life: "
        }
        if (questionIndex == 15){
            label.text = "I have been short of breath"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "I have been short of breath: "
        }
        if (questionIndex == 16){
            label.text = "I am losing weight"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "I am losing weight: "
        }
        if (questionIndex == 17){
            label.text = "I can't think clearly"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "I can't think clearly: "
        }
        if (questionIndex == 18){
            label.text = "I have been coughing"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "I have been coughing: "
        }
        if (questionIndex == 19){
            label.text = "I have a poor appetite"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "I have a poor appetite: "
        }
        if (questionIndex == 20){
            label.text = "I feel tightness in my chest"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "I feel tightness in my chest: "
        }
        if (questionIndex == 21){
            label.text = "Breathing is hard"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Breathing is hard: "
        }
        if (questionIndex == 22){
            label.isHidden = true
            button1.isHidden = true
            button2.isHidden = true
            button3.isHidden = true
            button4.isHidden = true
            button5.isHidden = true
            continueButton.isHidden = false
        }
    }
}
