//
//  SurveyPage1ViewController.swift
//  LungCancerSurvey
//
//  Created by Jonah on 6/4/21.
//

import Foundation
import UIKit

struct GlobalVariables {
    static var highestRatedSymptomLevel = 0
    static var lossOfAppetiteRating = 0
    static var brittleNailsRating = 0
    static var coughRating = 0
    static var lowEnergyLevelRating = 0
    static var diarrheaRating = 0
    static var difficultySleepingRating = 0
    static var dizzinessRating = 0
    static var drySkinRating = 0
    static var fatigueRating = 0
    static var hairLossRating = 0
    static var increasedBleedingRating = 0
    static var itchySkinRating = 0
    static var jaundiceRating = 0
    static var moreTripsRating = 0
    static var nauseaRating = 0
    static var numbnessRating = 0
    static var painRating = 0
    static var shortnessOfBreathRating = 0
    static var Page4WorstSymptomSelection = ""
    static var Page6FirstSelection = ""
    static var Page6SecondSelection = ""
    static var Page6ThirdSelection = ""
    static var SymptomTotalRating = 0
    static var CollabTotalRating = 0
    static var symptomOrder = ""
    static var symptomsExperiencedResponses = ""
    static var collabResponses = ""
    static var definitionOfTreatmentSuccess = ""
}

class SurveyPage1ViewController: UIViewController {
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var label: UILabel!
    var questionIndex = 0;
    @IBAction func selectButton1(_ sender: Any){
        if (questionIndex != 0){
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Not At All, "
        }
        button1.setTitle("Not At All", for: UIControl.State.normal)
        questionIndex += 1;
        updateLabel();
        button2.isHidden = false;
        button3.isHidden = false;
        button4.isHidden = false;
        
    }
    
    @IBAction func selectButton2(_ sender: Any){
        GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Mild, "
        questionIndex += 1;
        updateLabel();
        GlobalVariables.SymptomTotalRating += 1
    }
    
    @IBAction func selectButton3(_ sender: Any){
        GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Moderate, "
        questionIndex += 1;
        updateLabel();
        GlobalVariables.SymptomTotalRating += 2
    }
    
    @IBAction func selectButton4(_ sender: Any){
        GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Severe, "
        questionIndex += 1;
        updateLabel();
        GlobalVariables.SymptomTotalRating += 3
    }
    func updateLabel(){
        if (questionIndex == 1){
            label.text = "Overall Feeling/Fatigue"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Overall Feeling/Fatigue: "
        }
        if (questionIndex == 2){
            label.text = "Nausea"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Nausea: "
        }
        if (questionIndex == 3){
            label.text = "Vomiting"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Vomiting: "
        }
        if (questionIndex == 4){
            label.text = "Diarrhea"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Diarrhea: "
        }
        if (questionIndex == 5){
            label.text = "Constipation"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Constipation: "
        }
        if (questionIndex == 6){
            label.text = "Loss of appetite"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Loss of appetite: "
        }
        if (questionIndex == 7){
            label.text = "Cough"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Cough: "
        }
        if (questionIndex == 8){
            label.text = "Shortness of breath"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Shortness of breath: "
        }
        if (questionIndex == 9){
            label.text = "Skin problems"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Skin problems: "
        }
        if (questionIndex == 10){
            label.text = "Pain"
            GlobalVariables.symptomsExperiencedResponses = GlobalVariables.symptomsExperiencedResponses + "Pain: "
        }
        if (questionIndex == 11){
            label.isHidden = true
            button1.isHidden = true
            button2.isHidden = true
            button3.isHidden = true
            button4.isHidden = true
            continueButton.isHidden = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
