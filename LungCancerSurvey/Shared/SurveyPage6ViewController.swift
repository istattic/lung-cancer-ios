//
//  SurveyPage6ViewController.swift
//  LungCancerSurvey
//
//  Created by Jonah on 6/9/21.
//

import Foundation
import UIKit

class SurveyPage6ViewController: UIViewController{
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var button7: UIButton!
    @IBOutlet weak var button8: UIButton!
    @IBOutlet weak var button9: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var label: UILabel!
    var selectionIndex = 0
    @IBAction func selectButton1(_ sender: Any){
        button1.isHidden = true
        GlobalVariables.symptomOrder = GlobalVariables.symptomOrder + "Brittle Nails, "
        if (selectionIndex == 0){
            GlobalVariables.Page6FirstSelection = "Brittle Nails"
        }
        if (selectionIndex == 1){
            GlobalVariables.Page6SecondSelection = "Brittle Nails"
        }
        if (selectionIndex == 2){
            GlobalVariables.Page6ThirdSelection = "Brittle Nails"
        }
        selectionIndex += 1
        checkEmpty()
    }
    @IBAction func selectButton2(_ sender: Any){
        if (selectionIndex == 0){
            GlobalVariables.Page6FirstSelection = "Fatigue"
        }
        if (selectionIndex == 1){
            GlobalVariables.Page6SecondSelection = "Fatigue"
        }
        if (selectionIndex == 2){
            GlobalVariables.Page6ThirdSelection = "Fatigue"
        }
        button2.isHidden = true
        GlobalVariables.symptomOrder = GlobalVariables.symptomOrder + "Fatigue, "
        selectionIndex += 1
        checkEmpty()
    }
    @IBAction func selectButton3(_ sender: Any){
        if (selectionIndex == 0){
            GlobalVariables.Page6FirstSelection = "Dizziness"
        }
        if (selectionIndex == 1){
            GlobalVariables.Page6SecondSelection = "Dizziness"
        }
        if (selectionIndex == 2){
            GlobalVariables.Page6ThirdSelection = "Dizziness"
        }
        button3.isHidden = true
        GlobalVariables.symptomOrder = GlobalVariables.symptomOrder + "Dizziness, "
        selectionIndex += 1
        checkEmpty()
    }
    @IBAction func selectButton4(_ sender: Any){
        if (selectionIndex == 0){
            GlobalVariables.Page6FirstSelection = "Increased Bleeding"
        }
        if (selectionIndex == 1){
            GlobalVariables.Page6SecondSelection = "Increased Bleeding"
        }
        if (selectionIndex == 2){
            GlobalVariables.Page6ThirdSelection = "Increased Bleeding"
        }
        button4.isHidden = true
        GlobalVariables.symptomOrder = GlobalVariables.symptomOrder + "Increased Bleeding, "
        selectionIndex += 1
        checkEmpty()
    }
    @IBAction func selectButton5(_ sender: Any){
        if (selectionIndex == 0){
            GlobalVariables.Page6FirstSelection = "Yellow Skin"
        }
        if (selectionIndex == 1){
            GlobalVariables.Page6SecondSelection = "Yellow Skin"
        }
        if (selectionIndex == 2){
            GlobalVariables.Page6ThirdSelection = "Yellow Skin"
        }
        button5.isHidden = true
        GlobalVariables.symptomOrder = GlobalVariables.symptomOrder + "Yellow Skin, "
        selectionIndex += 1
        checkEmpty()
    }
    @IBAction func selectButton6(_ sender: Any){
        if (selectionIndex == 0){
            GlobalVariables.Page6FirstSelection = "More Trips to Clinic"
        }
        if (selectionIndex == 1){
            GlobalVariables.Page6SecondSelection = "More Trips to Clinic"
        }
        if (selectionIndex == 2){
            GlobalVariables.Page6ThirdSelection = "More Trips to Clinic"
        }
        button6.isHidden = true
        GlobalVariables.symptomOrder = GlobalVariables.symptomOrder + "More Trips to Clinic, "
        selectionIndex += 1
        checkEmpty()
    }
    @IBAction func selectButton7(_ sender: Any){
        if (selectionIndex == 0){
            GlobalVariables.Page6FirstSelection = "Numbness"
        }
        if (selectionIndex == 1){
            GlobalVariables.Page6SecondSelection = "Numbness"
        }
        if (selectionIndex == 2){
            GlobalVariables.Page6ThirdSelection = "Numbness"
        }
        button7.isHidden = true
        GlobalVariables.symptomOrder = GlobalVariables.symptomOrder + "Numbness, "
        selectionIndex += 1
        checkEmpty()
    }
    @IBAction func selectButton8(_ sender: Any){
        if (selectionIndex == 0){
            GlobalVariables.Page6FirstSelection = "Shortness of Breath"
        }
        if (selectionIndex == 1){
            GlobalVariables.Page6SecondSelection = "Shortness of Breath"
        }
        if (selectionIndex == 2){
            GlobalVariables.Page6ThirdSelection = "Shortness of Breath"
        }
        button8.isHidden = true
        GlobalVariables.symptomOrder = GlobalVariables.symptomOrder + "Shortness of Breath, "
        selectionIndex += 1
        checkEmpty()
    }
    @IBAction func selectButton9(_ sender: Any){
        if (selectionIndex == 0){
            GlobalVariables.Page6FirstSelection = "A Lot More Expensive"
        }
        if (selectionIndex == 1){
            GlobalVariables.Page6SecondSelection = "A Lot More Expensive"
        }
        if (selectionIndex == 2){
            GlobalVariables.Page6ThirdSelection = "A Lot More Expensive"
        }
        button9.isHidden = true
        GlobalVariables.symptomOrder = GlobalVariables.symptomOrder + "A Lot More Expensive, "
        selectionIndex += 1
        checkEmpty()
    }
    func checkEmpty (){
        if (button1.isHidden && button2.isHidden && button3.isHidden && button4.isHidden && button5.isHidden && button6.isHidden && button7.isHidden && button8.isHidden && button9.isHidden){
            continueButton.isHidden = false
        }
    }
}
