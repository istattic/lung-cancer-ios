//
//  SurveyPage4ViewController.swift
//  LungCancerSurvey
//
//  Created by Jonah on 6/9/21.
//

import Foundation
import UIKit

class SurveyPage4Controller: UIViewController {
        @IBOutlet weak var button1: UIButton!
        @IBOutlet weak var button2: UIButton!
        @IBOutlet weak var button3: UIButton!
        @IBOutlet weak var continueButton: UIButton!
        @IBOutlet weak var label: UILabel!
        var questionLabelIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (GlobalVariables.lossOfAppetiteRating == GlobalVariables.highestRatedSymptomLevel){
            button1.setTitle("Loss of Appetite", for: UIControl.State.normal)
        }
        if (GlobalVariables.brittleNailsRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Brittle Nails", for: UIControl.State.normal)
            } else {
                button2.setTitle("Brittle Nails", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.coughRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Cough", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Cough", for: UIControl.State.normal)
            } else {
                button3.setTitle("Cough", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.lowEnergyLevelRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Low Energy", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Low Energy", for: UIControl.State.normal)
            } else {
                button3.setTitle("Low Energy", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.diarrheaRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Diarrhea", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Diarrhea", for: UIControl.State.normal)
            } else {
                button3.setTitle("Diarrhea", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.difficultySleepingRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Difficulty Sleeping", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Difficulty Sleeping", for: UIControl.State.normal)
            } else {
                button3.setTitle("Difficulty Sleeping", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.dizzinessRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Dizziness", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Dizziness", for: UIControl.State.normal)
            } else {
                button3.setTitle("Dizziness", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.drySkinRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Dry Skin", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Dry Skin", for: UIControl.State.normal)
            } else {
                button3.setTitle("Dry Skin", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.fatigueRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Fatigue", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Fatigue", for: UIControl.State.normal)
            } else {
                button3.setTitle("Fatigue", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.hairLossRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Hair Loss", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Hair Loss", for: UIControl.State.normal)
            } else {
                button3.setTitle("Hair Loss", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.increasedBleedingRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Increased Bleeding", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Increased Bleeding", for: UIControl.State.normal)
            } else {
                button3.setTitle("Increased Bleeding", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.itchySkinRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Itchy Skin", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Itchy Skin", for: UIControl.State.normal)
            } else {
                button3.setTitle("Itchy Skin", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.jaundiceRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Yellow Skin", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Yellow Skin", for: UIControl.State.normal)
            } else {
                button3.setTitle("Yellow Skin", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.moreTripsRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("More Chemo Trips", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("More Chemo Trips", for: UIControl.State.normal)
            } else {
                button3.setTitle("More Chemo Trips", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.nauseaRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Nausea", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Nausea", for: UIControl.State.normal)
            } else {
                button3.setTitle("Nausea", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.numbnessRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Numbness", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Numbness", for: UIControl.State.normal)
            } else {
                button3.setTitle("Numbness", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.painRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Pain", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Pain", for: UIControl.State.normal)
            } else {
                button3.setTitle("Pain", for: UIControl.State.normal)
            }
            
        }
        if (GlobalVariables.shortnessOfBreathRating == GlobalVariables.highestRatedSymptomLevel){
            if (button1.title(for: UIControl.State.normal) == "Test"){
                button1.setTitle("Shortness of Breath", for: UIControl.State.normal)
            } else if (button2.title(for: UIControl.State.normal) == "Test"){
                button2.setTitle("Shortness of Breath", for: UIControl.State.normal)
            } else {
                button3.setTitle("Shortness of Breath", for: UIControl.State.normal)
            }
            
        }
        if (button2.title(for: UIControl.State.normal) == "Test"){
            button2.isHidden = true
        }
        if (button3.title(for: UIControl.State.normal) == "Test"){
            button3.isHidden = true
        }
    }
    
    @IBAction func selectButton1(_ sender: Any){
        if (questionLabelIndex == 0){
            GlobalVariables.Page4WorstSymptomSelection = button1.title(for: UIControl.State.normal)!
        }
        questionLabelIndex += 1
        UpdateLabel()
    }
    @IBAction func selectButton2(_ sender: Any){
        if (questionLabelIndex == 0){
            GlobalVariables.Page4WorstSymptomSelection = button2.title(for: UIControl.State.normal)!
        }
        questionLabelIndex += 1
        UpdateLabel()
    }
    @IBAction func selectButton3(_ sender: Any){
        if (questionLabelIndex == 0){
            GlobalVariables.Page4WorstSymptomSelection = button3.title(for: UIControl.State.normal)!
        }
        questionLabelIndex += 1
        UpdateLabel()
    }
    func UpdateLabel(){
        if (questionLabelIndex == 1){
            label.text = "To reach an important personal goal, how long would you be willing to tolerate a higher change of uncomfortable side effects of chemo?"
            button1.setTitle("No Time Period", for: UIControl.State.normal)
            button2.setTitle("Months", for: UIControl.State.normal)
            button3.setTitle("Years", for: UIControl.State.normal)
            button3.isHidden = false
            button2.isHidden = false
            button1.isHidden = false
        }
        if (questionLabelIndex == 2){
            label.text = "Would you be willing to tolerate all of the side effects previously mentioned if it meant you might live longer?"
            button1.setTitle("Yes", for: UIControl.State.normal)
            button2.setTitle("No", for: UIControl.State.normal)
            button3.setTitle("Don't Know", for: UIControl.State.normal)
            button3.isHidden = false
            button2.isHidden = false
            button1.isHidden = false
        }
        if (questionLabelIndex == 3){
            continueButton.isHidden = false
            button1.isHidden = true
            button2.isHidden = true
            button3.isHidden = true
        }
    }
}
