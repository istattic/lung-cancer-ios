//
//  SurveyPage2ViewController.swift
//  LungCancerSurvey
//
//  Created by Jonah on 6/4/21.
//

import Foundation
import UIKit

class SurveyPage2ViewController: UIViewController {
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var label: UILabel!
    var questionIndex = 0;
    @IBAction func selectButton1(_ sender: Any){
        GlobalVariables.collabResponses = GlobalVariables.collabResponses + "None, "
        questionIndex += 1;
        updateLabel();
    }
    
    @IBAction func selectButton2(_ sender: Any){
        GlobalVariables.collabResponses = GlobalVariables.collabResponses + "A Little, "
        GlobalVariables.CollabTotalRating += 1
        questionIndex += 1;
        updateLabel();
    }
    
    @IBAction func selectButton3(_ sender: Any){
        GlobalVariables.collabResponses = GlobalVariables.collabResponses + "Some, "
        GlobalVariables.CollabTotalRating += 2
        questionIndex += 1;
        updateLabel();
    }
    
    @IBAction func selectButton4(_ sender: Any){
        GlobalVariables.collabResponses = GlobalVariables.collabResponses + "A Lot, "
        GlobalVariables.CollabTotalRating += 3
        questionIndex += 1;
        updateLabel();
    }
    @IBAction func selectButton5(_ sender: Any){
        GlobalVariables.collabResponses = GlobalVariables.collabResponses + "Every Effort, "
        GlobalVariables.CollabTotalRating += 4
        questionIndex += 1;
        updateLabel();
    }
    func updateLabel(){
        if (questionIndex == 1){
            label.text = "How much effort was made to listen to the things that matter most to you about your health issues?"
            GlobalVariables.collabResponses = GlobalVariables.collabResponses + "How much effort was made to listen to the things that matter most to you about your health issues: "
        }
        if (questionIndex == 2){
            label.text = "How much effort was made to include what matters most to you in choosing what to do next?"
            GlobalVariables.collabResponses = GlobalVariables.collabResponses + "How much effort was made to include what matters most to you in choosing what to do next: "
        }
        if (questionIndex == 3){
            label.isHidden = true
            button1.isHidden = true
            button2.isHidden = true
            button3.isHidden = true
            button4.isHidden = true
            button5.isHidden = true
            continueButton.isHidden = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
