//
//  LungCancerSurveyApp.swift
//  Shared
//
//  Created by Jonah on 6/4/21.
//

import UIKit

import SwiftUI

@main
struct testApp: App {
    var body: some Scene {
        WindowGroup {
            StoryboardViewController()
        }
    }
}
struct StoryboardViewController: UIViewControllerRepresentable {
    func makeUIViewController(context: Context) -> some UIViewController {
        let storyboard = UIStoryboard(name: "main", bundle: Bundle.main)
        let controller = storyboard.instantiateViewController(identifier: "SurveyPage1ViewController")
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
}


