//
//  WrittenFeedbackViewController.swift
//  LungCancerSurvey
//
//  Created by Jonah on 6/10/21.
//

import Foundation
import UIKit

class WrittenFeedbackViewController: UIViewController{
    @IBOutlet weak var definitionResponse: UITextField!
    @IBAction func submitResponse(_ sender: Any?){
        GlobalVariables.definitionOfTreatmentSuccess = definitionResponse.text!
    }
}
