//
//  ResultsPageViewController.swift
//  LungCancerSurvey
//
//  Created by Jonah on 6/10/21.
//

import Foundation
import UIKit

class ResultsPageViewController: UIViewController{
    @IBOutlet weak var symptomRatingBar: UIProgressView!
    @IBOutlet weak var collabRatingBar: UIProgressView!
    @IBOutlet weak var symptom1: UILabel!
    @IBOutlet weak var symptom2: UILabel!
    @IBOutlet weak var symptom3: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //ranking symptoms labels
        symptom1.text = GlobalVariables.Page4WorstSymptomSelection
        if (symptom1.text == GlobalVariables.Page6FirstSelection){
            symptom2.text = GlobalVariables.Page6SecondSelection
        } else {
            symptom2.text = GlobalVariables.Page6FirstSelection
        }
        if (symptom2.text == GlobalVariables.Page6SecondSelection){
            symptom3.text = GlobalVariables.Page6ThirdSelection
        } else {
            symptom3.text = GlobalVariables.Page6SecondSelection
        }
        
        //symptom progress bar
        if (GlobalVariables.SymptomTotalRating < 5){
            symptomRatingBar.progress = 0.1
            symptomRatingBar.tintColor = UIColor.green
        } else if (GlobalVariables.SymptomTotalRating < 10){
            symptomRatingBar.progress = 0.2
            symptomRatingBar.tintColor = UIColor.green
        } else if (GlobalVariables.SymptomTotalRating < 15){
            symptomRatingBar.progress = 0.3
            symptomRatingBar.tintColor = UIColor.yellow
        } else if (GlobalVariables.SymptomTotalRating < 20){
            symptomRatingBar.progress = 0.5
            symptomRatingBar.tintColor = UIColor.yellow
        } else if (GlobalVariables.SymptomTotalRating < 25){
            symptomRatingBar.progress = 0.6
            symptomRatingBar.tintColor = UIColor.orange
        } else if (GlobalVariables.SymptomTotalRating < 30){
            symptomRatingBar.progress = 0.8
            symptomRatingBar.tintColor = UIColor.red
        } else {
            symptomRatingBar.progress = 1
            symptomRatingBar.tintColor = UIColor.red
        }
        
        
        //collaboration progress bar
        if (GlobalVariables.CollabTotalRating < 4){
            collabRatingBar.progress = 1
            collabRatingBar.tintColor = UIColor.red
        } else if (GlobalVariables.CollabTotalRating < 7){
            collabRatingBar.progress = 0.7
            collabRatingBar.tintColor = UIColor.orange
        } else if (GlobalVariables.CollabTotalRating < 10){
            collabRatingBar.progress = 0.4
            collabRatingBar.tintColor = UIColor.yellow
        } else {
            collabRatingBar.progress = 0.1
            collabRatingBar.tintColor = UIColor.green
        }
    
        //airtable data post
        
        let urlString = "https://api.airtable.com/v0/appwKffEWbOCSOaGh/table1"
        let url = URL(string: urlString)!
        

        var request = URLRequest(url: url)
        request.addValue("Bearer keyd27XwnU8As7tN9", forHTTPHeaderField: "Authorization")
        request.addValue("Application/json", forHTTPHeaderField: "Accept")
        request.addValue("Application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST";
        let date = Date()

        let currentDateTime = date.description + " (IN UST)"
        let parameters: [String: Any] = [
            "fields": [
                "Timestamp" : currentDateTime,
                "UnwantedSymptomsOrder" : GlobalVariables.symptomOrder,
                "SymptomsExperiencedResponses" : GlobalVariables.symptomsExperiencedResponses,
                "CollabResponses" : GlobalVariables.collabResponses,
                "TheirDefinitionOfTreatmentSuccess" : GlobalVariables.definitionOfTreatmentSuccess
            ],
            "typecast": true
        ]
    
        guard let jsonData = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {
            return
        }
        print(parameters)
        print(jsonData)
        request.httpBody = jsonData
        // Make your request and handle the response
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error {
                print("Error with fetching films: \(error)")
                return
              }
              
              guard let httpResponse = response as? HTTPURLResponse,
                    (200...299).contains(httpResponse.statusCode) else {
                print("Error with the response, unexpected status code: \(response)")
                return
              }
            })
            task.resume()
    
    }
}
