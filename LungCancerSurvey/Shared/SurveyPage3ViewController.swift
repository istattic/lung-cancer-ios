//
//  SurveyPage3ViewController.swift
//  LungCancerSurvey
//
//  Created by Jonah on 6/9/21.
//

import Foundation
import UIKit

class SurveyPage3ViewController: UIViewController {
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var button7: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var label: UILabel!
    var questionIndex = 0;
    @IBAction func selectButton1(_ sender: Any){
        if (questionIndex == 1){
            GlobalVariables.lossOfAppetiteRating = 1
        }
        if (questionIndex == 2){
            GlobalVariables.brittleNailsRating = 1
        }
        if (questionIndex == 3){
            GlobalVariables.coughRating = 1
        }
        if (questionIndex == 4){
            GlobalVariables.lowEnergyLevelRating = 1
        }
        if (questionIndex == 5){
            GlobalVariables.diarrheaRating = 1
        }
        if (questionIndex == 6){
            GlobalVariables.difficultySleepingRating = 1
        }
        if (questionIndex == 7){
            GlobalVariables.dizzinessRating = 1
        }
        if (questionIndex == 8){
            GlobalVariables.drySkinRating = 1
        }
        if (questionIndex == 9){
            GlobalVariables.fatigueRating = 1
        }
        if (questionIndex == 10){
            GlobalVariables.hairLossRating = 1
        }
        if (questionIndex == 11){
            GlobalVariables.increasedBleedingRating = 1
        }
        if (questionIndex == 12){
            GlobalVariables.itchySkinRating = 1
        }
        if (questionIndex == 13){
            GlobalVariables.jaundiceRating = 1
        }
        if (questionIndex == 14){
            GlobalVariables.moreTripsRating = 1
        }
        if (questionIndex == 15){
            GlobalVariables.nauseaRating = 1
        }
        if (questionIndex == 16){
            GlobalVariables.numbnessRating = 1
        }
        if (questionIndex == 17){
            GlobalVariables.painRating = 1
        }
        if (questionIndex == 18){
            GlobalVariables.shortnessOfBreathRating = 1
        }
        
        
        questionIndex += 1;
        if (GlobalVariables.highestRatedSymptomLevel < 1){
            GlobalVariables.highestRatedSymptomLevel = 1
        }
        updateLabel();
    }
    
    @IBAction func selectButton2(_ sender: Any){
        if (questionIndex == 1){
            GlobalVariables.lossOfAppetiteRating = 2
        }
        if (questionIndex == 2){
            GlobalVariables.brittleNailsRating = 2
        }
        if (questionIndex == 3){
            GlobalVariables.coughRating = 2
        }
        if (questionIndex == 4){
            GlobalVariables.lowEnergyLevelRating = 2
        }
        if (questionIndex == 5){
            GlobalVariables.diarrheaRating = 2
        }
        if (questionIndex == 6){
            GlobalVariables.difficultySleepingRating = 2
        }
        if (questionIndex == 7){
            GlobalVariables.dizzinessRating = 2
        }
        if (questionIndex == 8){
            GlobalVariables.drySkinRating = 2
        }
        if (questionIndex == 9){
            GlobalVariables.fatigueRating = 2
        }
        if (questionIndex == 10){
            GlobalVariables.hairLossRating = 2
        }
        if (questionIndex == 11){
            GlobalVariables.increasedBleedingRating = 2
        }
        if (questionIndex == 12){
            GlobalVariables.itchySkinRating = 2
        }
        if (questionIndex == 13){
            GlobalVariables.jaundiceRating = 2
        }
        if (questionIndex == 14){
            GlobalVariables.moreTripsRating = 2
        }
        if (questionIndex == 15){
            GlobalVariables.nauseaRating = 2
        }
        if (questionIndex == 16){
            GlobalVariables.numbnessRating = 2
        }
        if (questionIndex == 17){
            GlobalVariables.painRating = 2
        }
        if (questionIndex == 18){
            GlobalVariables.shortnessOfBreathRating = 2
        }
        
        
        questionIndex += 1;
        if (GlobalVariables.highestRatedSymptomLevel < 2){
            GlobalVariables.highestRatedSymptomLevel = 2
        }
        updateLabel();
    }
    
    @IBAction func selectButton3(_ sender: Any){
        if (questionIndex == 1){
            GlobalVariables.lossOfAppetiteRating = 3
        }
        if (questionIndex == 2){
            GlobalVariables.brittleNailsRating = 3
        }
        if (questionIndex == 3){
            GlobalVariables.coughRating = 3
        }
        if (questionIndex == 4){
            GlobalVariables.lowEnergyLevelRating = 3
        }
        if (questionIndex == 5){
            GlobalVariables.diarrheaRating = 3
        }
        if (questionIndex == 6){
            GlobalVariables.difficultySleepingRating = 3
        }
        if (questionIndex == 7){
            GlobalVariables.dizzinessRating = 3
        }
        if (questionIndex == 8){
            GlobalVariables.drySkinRating = 3
        }
        if (questionIndex == 9){
            GlobalVariables.fatigueRating = 3
        }
        if (questionIndex == 10){
            GlobalVariables.hairLossRating = 3
        }
        if (questionIndex == 11){
            GlobalVariables.increasedBleedingRating = 3
        }
        if (questionIndex == 12){
            GlobalVariables.itchySkinRating = 3
        }
        if (questionIndex == 13){
            GlobalVariables.jaundiceRating = 3
        }
        if (questionIndex == 14){
            GlobalVariables.moreTripsRating = 3
        }
        if (questionIndex == 15){
            GlobalVariables.nauseaRating = 3
        }
        if (questionIndex == 16){
            GlobalVariables.numbnessRating = 3
        }
        if (questionIndex == 17){
            GlobalVariables.painRating = 3
        }
        if (questionIndex == 18){
            GlobalVariables.shortnessOfBreathRating = 3
        }
        
        
        questionIndex += 1;
        if (GlobalVariables.highestRatedSymptomLevel < 3){
            GlobalVariables.highestRatedSymptomLevel = 3
        }
        updateLabel();
    }
    
    @IBAction func selectButton4(_ sender: Any){
        if (questionIndex == 1){
            GlobalVariables.lossOfAppetiteRating = 4
        }
        if (questionIndex == 2){
            GlobalVariables.brittleNailsRating = 4
        }
        if (questionIndex == 3){
            GlobalVariables.coughRating = 4
        }
        if (questionIndex == 4){
            GlobalVariables.lowEnergyLevelRating = 4
        }
        if (questionIndex == 5){
            GlobalVariables.diarrheaRating = 4
        }
        if (questionIndex == 6){
            GlobalVariables.difficultySleepingRating = 4
        }
        if (questionIndex == 7){
            GlobalVariables.dizzinessRating = 4
        }
        if (questionIndex == 8){
            GlobalVariables.drySkinRating = 4
        }
        if (questionIndex == 9){
            GlobalVariables.fatigueRating = 4
        }
        if (questionIndex == 10){
            GlobalVariables.hairLossRating = 4
        }
        if (questionIndex == 11){
            GlobalVariables.increasedBleedingRating = 4
        }
        if (questionIndex == 12){
            GlobalVariables.itchySkinRating = 4
        }
        if (questionIndex == 13){
            GlobalVariables.jaundiceRating = 4
        }
        if (questionIndex == 14){
            GlobalVariables.moreTripsRating = 4
        }
        if (questionIndex == 15){
            GlobalVariables.nauseaRating = 4
        }
        if (questionIndex == 16){
            GlobalVariables.numbnessRating = 4
        }
        if (questionIndex == 17){
            GlobalVariables.painRating = 4
        }
        if (questionIndex == 18){
            GlobalVariables.shortnessOfBreathRating = 4
        }
        
        
        questionIndex += 1;
        if (GlobalVariables.highestRatedSymptomLevel < 4){
            GlobalVariables.highestRatedSymptomLevel = 4
        }
        updateLabel();
    }
    @IBAction func selectButton5(_ sender: Any){
        if (questionIndex == 1){
            GlobalVariables.lossOfAppetiteRating = 5
        }
        if (questionIndex == 2){
            GlobalVariables.brittleNailsRating = 5
        }
        if (questionIndex == 3){
            GlobalVariables.coughRating = 5
        }
        if (questionIndex == 4){
            GlobalVariables.lowEnergyLevelRating = 5
        }
        if (questionIndex == 5){
            GlobalVariables.diarrheaRating = 5
        }
        if (questionIndex == 6){
            GlobalVariables.difficultySleepingRating = 5
        }
        if (questionIndex == 7){
            GlobalVariables.dizzinessRating = 5
        }
        if (questionIndex == 8){
            GlobalVariables.drySkinRating = 5
        }
        if (questionIndex == 9){
            GlobalVariables.fatigueRating = 5
        }
        if (questionIndex == 10){
            GlobalVariables.hairLossRating = 5
        }
        if (questionIndex == 11){
            GlobalVariables.increasedBleedingRating = 5
        }
        if (questionIndex == 12){
            GlobalVariables.itchySkinRating = 5
        }
        if (questionIndex == 13){
            GlobalVariables.jaundiceRating = 5
        }
        if (questionIndex == 14){
            GlobalVariables.moreTripsRating = 5
        }
        if (questionIndex == 15){
            GlobalVariables.nauseaRating = 5
        }
        if (questionIndex == 16){
            GlobalVariables.numbnessRating = 5
        }
        if (questionIndex == 17){
            GlobalVariables.painRating = 5
        }
        if (questionIndex == 18){
            GlobalVariables.shortnessOfBreathRating = 5
        }
        
        
        questionIndex += 1;
        if (GlobalVariables.highestRatedSymptomLevel < 5){
            GlobalVariables.highestRatedSymptomLevel = 5
        }
        updateLabel();
    }
    @IBAction func selectButton6(_ sender: Any){
        if (questionIndex == 1){
            GlobalVariables.lossOfAppetiteRating = 6
        }
        if (questionIndex == 2){
            GlobalVariables.brittleNailsRating = 6
        }
        if (questionIndex == 3){
            GlobalVariables.coughRating = 6
        }
        if (questionIndex == 4){
            GlobalVariables.lowEnergyLevelRating = 6
        }
        if (questionIndex == 5){
            GlobalVariables.diarrheaRating = 6
        }
        if (questionIndex == 6){
            GlobalVariables.difficultySleepingRating = 6
        }
        if (questionIndex == 7){
            GlobalVariables.dizzinessRating = 6
        }
        if (questionIndex == 8){
            GlobalVariables.drySkinRating = 6
        }
        if (questionIndex == 9){
            GlobalVariables.fatigueRating = 6
        }
        if (questionIndex == 10){
            GlobalVariables.hairLossRating = 6
        }
        if (questionIndex == 11){
            GlobalVariables.increasedBleedingRating = 6
        }
        if (questionIndex == 12){
            GlobalVariables.itchySkinRating = 6
        }
        if (questionIndex == 13){
            GlobalVariables.jaundiceRating = 6
        }
        if (questionIndex == 14){
            GlobalVariables.moreTripsRating = 6
        }
        if (questionIndex == 15){
            GlobalVariables.nauseaRating = 6
        }
        if (questionIndex == 16){
            GlobalVariables.numbnessRating = 6
        }
        if (questionIndex == 17){
            GlobalVariables.painRating = 6
        }
        if (questionIndex == 18){
            GlobalVariables.shortnessOfBreathRating = 6
        }
        
        
        questionIndex += 1;
        if (GlobalVariables.highestRatedSymptomLevel < 6){
            GlobalVariables.highestRatedSymptomLevel = 6
        }
        updateLabel();
    }
    @IBAction func selectButton7(_ sender: Any){
        if (questionIndex == 1){
            GlobalVariables.lossOfAppetiteRating = 7
        }
        if (questionIndex == 2){
            GlobalVariables.brittleNailsRating = 7
        }
        if (questionIndex == 3){
            GlobalVariables.coughRating = 7
        }
        if (questionIndex == 4){
            GlobalVariables.lowEnergyLevelRating = 7
        }
        if (questionIndex == 5){
            GlobalVariables.diarrheaRating = 7
        }
        if (questionIndex == 6){
            GlobalVariables.difficultySleepingRating = 7
        }
        if (questionIndex == 7){
            GlobalVariables.dizzinessRating = 7
        }
        if (questionIndex == 8){
            GlobalVariables.drySkinRating = 7
        }
        if (questionIndex == 9){
            GlobalVariables.fatigueRating = 7
        }
        if (questionIndex == 10){
            GlobalVariables.hairLossRating = 7
        }
        if (questionIndex == 11){
            GlobalVariables.increasedBleedingRating = 7
        }
        if (questionIndex == 12){
            GlobalVariables.itchySkinRating = 7
        }
        if (questionIndex == 13){
            GlobalVariables.jaundiceRating = 7
        }
        if (questionIndex == 14){
            GlobalVariables.moreTripsRating = 7
        }
        if (questionIndex == 15){
            GlobalVariables.nauseaRating = 7
        }
        if (questionIndex == 16){
            GlobalVariables.numbnessRating = 7
        }
        if (questionIndex == 17){
            GlobalVariables.painRating = 7
        }
        if (questionIndex == 18){
            GlobalVariables.shortnessOfBreathRating = 7
        }
        
        
        questionIndex += 1;
        if (GlobalVariables.highestRatedSymptomLevel < 7){
            GlobalVariables.highestRatedSymptomLevel = 7
        }
        updateLabel();
    }
    func updateLabel(){
        if (questionIndex == 1){
            label.text = "Loss of Appetite"
            button1.setTitle("1", for: UIControl.State.normal)
            button2.isHidden = false
            button3.isHidden = false
            button4.isHidden = false
            button5.isHidden = false
            button6.isHidden = false
            button7.isHidden = false
        }
        if (questionIndex == 2){
            label.text = "Brittle Nails"
        }
        if (questionIndex == 3){
            label.text = "Cough"
        }
        if (questionIndex == 4){
            label.text = "Decreased Energy Level"
        }
        if (questionIndex == 5){
            label.text = "Diarrhea"
        }
        if (questionIndex == 6){
            label.text = "Difficulty Sleeping"
        }
        if (questionIndex == 7){
            label.text = "Dizziness"
        }
        if (questionIndex == 8){
            label.text = "Dry/Peeling Skin"
        }
        if (questionIndex == 9){
            label.text = "Fatigue"
        }
        if (questionIndex == 10){
            label.text = "Hair Loss"
        }
        if (questionIndex == 11){
            label.text = "Increased Chance of Bleeding"
        }
        if (questionIndex == 12){
            label.text = "Itchy Skin"
        }
        if (questionIndex == 13){
            label.text = "Jaundice (yellow skin)"
        }
        if (questionIndex == 14){
            label.text = "More Trips to Clinic for Chemo"
        }
        if (questionIndex == 15){
            label.text = "Nausea (sick to stomach)"
        }
        if (questionIndex == 16){
            label.text = "Numbness and/or Tingling"
        }
        if (questionIndex == 17){
            label.text = "Pain"
        }
        if (questionIndex == 18){
            label.text = "Shortness of Breath"
        }
        if (questionIndex == 19){
            label.isHidden = true
            button1.isHidden = true
            button2.isHidden = true
            button3.isHidden = true
            button4.isHidden = true
            button5.isHidden = true
            button6.isHidden = true
            button7.isHidden = true
            continueButton.isHidden = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
